﻿namespace MoveDatabases
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.progressText = new System.Windows.Forms.TextBox();
            this.alreadyExistsLabel = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1563, 767);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(157, 46);
            this.button1.TabIndex = 0;
            this.button1.Text = "Move Database";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // progressText
            // 
            this.progressText.Location = new System.Drawing.Point(25, 24);
            this.progressText.Multiline = true;
            this.progressText.Name = "progressText";
            this.progressText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.progressText.Size = new System.Drawing.Size(1709, 700);
            this.progressText.TabIndex = 1;
            // 
            // alreadyExistsLabel
            // 
            this.alreadyExistsLabel.Location = new System.Drawing.Point(42, 767);
            this.alreadyExistsLabel.Multiline = true;
            this.alreadyExistsLabel.Name = "alreadyExistsLabel";
            this.alreadyExistsLabel.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.alreadyExistsLabel.Size = new System.Drawing.Size(1505, 174);
            this.alreadyExistsLabel.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 747);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(334, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Databases already existing in the destination server";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(379, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Progress of the databases that are applicable for the move";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1761, 971);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.alreadyExistsLabel);
            this.Controls.Add(this.progressText);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox progressText;
        private System.Windows.Forms.TextBox alreadyExistsLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}