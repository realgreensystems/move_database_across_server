﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace MoveDatabases
{
    public partial class Form1 : Form
    {
        public string sql = @"SELECT sd.NAME
,smf.NAME
,smf.type_desc
,(CAST(smf.size AS FLOAT) * 8096) AS SizeBytes
,(CAST(smf.size AS FLOAT) * 8096) / (1024) AS SizeKB
,(CAST(smf.size AS FLOAT) * 8096) / (1024 * 1024) AS SizeMB
,(CAST(smf.size AS FLOAT) * 8096) / (1024 * 1024 * 1024) AS SizeGB
,smf.physical_name
,sd.log_reuse_wait_desc
,sd.recovery_model_desc
,*
FROM sys.databases sd
INNER JOIN sys.master_files smf ON sd.database_id = smf.database_id
WHERE
smf.type_desc IN (
    'ROWS'
    ,'LOG'
    )
and sd.database_id > 4
ORDER BY sd.[name] asc";
        static void Main()
        {
            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());

        }
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var destDataDirectory = ConfigurationManager.AppSettings["destinationDataDirectory"];
            var destLogDirectory = ConfigurationManager.AppSettings["destinationLogDirectory"];
            var pathReplacement = ConfigurationManager.AppSettings["sourcedirectory"];
            List<DataBaseObjects> results = new List<DataBaseObjects>();
            var destinationDatabases = GetDestinationDatabases();
            using (var cmd = DatabaseService.CreateCommand())
            {

                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Clear();

                cmd.CommandText = sql;

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var model = new DataBaseObjects();
                        model.Name = reader["NAME"].ToString();
                        var existModel = results.Where(x => x.Name.ToUpper().Trim() == model.Name.Trim().ToUpper()).FirstOrDefault();
                        if(existModel != null)
                        {
                            model = existModel;
                        }
                        else
                        {
                            results.Add(model);
                        }
                        model.DataFile = false;
                        model.TypeDescription = reader["type_desc"].ToString().Trim().ToUpper();
                        if (model.TypeDescription == "ROWS")
                            model.DataFile = true;


                        if(model.DataFile)
                        {
                            model.FilePath = reader["physical_name"].ToString();
                            model.ModifiedFilePath = CleanseFolderPath(model.FilePath, pathReplacement);
                        }
                        else
                        {
                            model.LogFilePath = reader["physical_name"].ToString();
                            model.ModifiedLogFilePath = CleanseFolderPath(model.LogFilePath, pathReplacement);
                        }
                        
                    }
                }
                progressText.Text += Environment.NewLine + results.Count().ToString() + " Databases to move";
                progressText.Refresh();
                foreach (var database in results)
                {
                    var destinationDataBaseObject = destinationDatabases.Where(x => x.Name.Trim().ToUpper() == database.Name.Trim().ToUpper()).FirstOrDefault();
                    if (destinationDataBaseObject == null)
                    {
                        using (EventLog eventLog = new EventLog("Application"))
                        {
                            eventLog.Source = "Application";
                            eventLog.WriteEntry(DateTime.Now.ToString() + ". Started processing DataBase " + database.Name, EventLogEntryType.Information, 101, 1);
                        }

                        var dataFileName = Path.GetFileName(database.FilePath);
                        var logFileName = Path.GetFileName(database.LogFilePath);


                        var singleUser = "ALTER DATABASE [" + database.Name + "] SET  SINGLE_USER WITH ROLLBACK IMMEDIATE; ";
                        var detachDatabase = "EXEC master.dbo.sp_detach_db @dbname = N'" + database.Name + "'; ";

                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.Clear();
                        cmd.CommandText = singleUser;
                        cmd.ExecuteNonQuery();

                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.Clear();
                        cmd.CommandText = detachDatabase;
                        cmd.ExecuteNonQuery();
                        
                        progressText.Text += Environment.NewLine + "Moving " + database.ModifiedFilePath + " to " + destDataDirectory + dataFileName;
                        //progressText.Text += Environment.NewLine + "Moving " + database.ModifiedLogFilePath + " to " + destLogDirectory + logFileName;
                        progressText.Refresh();
                        File.Copy(database.ModifiedFilePath, destDataDirectory + dataFileName);
                        //File.Copy(database.ModifiedLogFilePath, destLogDirectory + logFileName);

                        progressText.Text += Environment.NewLine + "Creating the destination database " + DateTime.Now.ToString();
                        progressText.Refresh();
                        CreateDestinationDatabase(database);
                        progressText.Text += Environment.NewLine + "Finished creating the destination database " + DateTime.Now.ToString();
                        progressText.Refresh();
                        using (EventLog eventLog = new EventLog("Application"))
                        {
                            eventLog.Source = "Application";
                            eventLog.WriteEntry(DateTime.Now.ToString() + ". Finished restoring Destination DataBase " + database.Name, EventLogEntryType.Information, 101, 1);
                        }
                        var attachDatabase = @"CREATE DATABASE  [" + database.Name + @"] ON (FILENAME = '" + database.FilePath + @"'),  (FILENAME = '" + database.LogFilePath + "') FOR ATTACH;   ";
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.Clear();
                        cmd.CommandText = attachDatabase;
                        cmd.ExecuteNonQuery();
                        using (EventLog eventLog = new EventLog("Application"))
                        {
                            eventLog.Source = "Application";
                            eventLog.WriteEntry(DateTime.Now.ToString() + ". Reattached Source DataBase " + database.Name, EventLogEntryType.Information, 101, 1);
                        }
                    }
                    else
                    {
                        alreadyExistsLabel.Text += Environment.NewLine + database.Name + " Already moved to the destination server ";
                        alreadyExistsLabel.Refresh();
                    }
                }
            }
        }
        private void CreateDestinationDatabase(DataBaseObjects database)
        {
            var destDataDirectory = ConfigurationManager.AppSettings["destinationDataDirectory"];
            var destLogDirectory = ConfigurationManager.AppSettings["destinationLogDirectory"];
            var pathReplacement = ConfigurationManager.AppSettings["sourcedirectory"];
            List<DataBaseObjects> results = new List<DataBaseObjects>();
            using (var cmd = DatabaseService.CreateCommand(true))
            {
                var dataFileName = Path.GetFileName(database.FilePath);
                var logFileName = Path.GetFileName(database.LogFilePath);

                var attachDatabase = @"CREATE DATABASE  [" + database.Name + @"] ON (FILENAME = '" + destDataDirectory + dataFileName + @"')  FOR ATTACH;   ";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Clear();
                cmd.CommandText = attachDatabase;
                cmd.ExecuteNonQuery();
            }
        }

        private List<DataBaseObjects> GetDestinationDatabases()
        {
            List<DataBaseObjects> results = new List<DataBaseObjects>();
            using (var cmd = DatabaseService.CreateCommand(true))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Clear();

                cmd.CommandText = sql;

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var model = new DataBaseObjects();
                        model.Name = reader["NAME"].ToString();
                        var existModel = results.Where(x => x.Name.ToUpper().Trim() == model.Name.Trim().ToUpper()).FirstOrDefault();
                        if (existModel != null)
                        {
                            model = existModel;
                        }
                        else
                        {
                            results.Add(model);
                        }
                        model.DataFile = false;
                        model.TypeDescription = reader["type_desc"].ToString().Trim().ToUpper();
                        if (model.TypeDescription == "ROWS")
                            model.DataFile = true;


                    }
                }
            }
            return results;
        }
        public class DataBaseObjects
        {
            public string Name { get; set; }
            public string FilePath { get; set; }
            public string ModifiedFilePath { get; set; }
            public string LogFilePath { get; set; }
            public string ModifiedLogFilePath { get; set; }
            public string TypeDescription { get; set; }
            public bool DataFile { get; set; }
        }
        public string CleanseFolderPath(string pathToCleanse, string pathReplacement)
        {
            string returnPath = "";
            if (pathToCleanse.StartsWith("C:"))
            {
                returnPath = pathReplacement + pathToCleanse.Replace("C:", "C$");
            }
            else if (pathToCleanse.StartsWith("D:"))
            {
                returnPath = pathReplacement + pathToCleanse.Replace("D:", "D$");
            }
            else if (pathToCleanse.StartsWith("E:"))
            {
                returnPath = pathReplacement + pathToCleanse.Replace("E:", "E$");
            }
            else if (pathToCleanse.StartsWith("F:"))
            {
                returnPath = pathReplacement + pathToCleanse.Replace("F:", "F$");
            }
            else if (pathToCleanse.StartsWith("G:"))
            {
                returnPath = pathReplacement + pathToCleanse.Replace("G:", "G$");
            }
            else if (pathToCleanse.StartsWith("c:"))
            {
                returnPath = pathReplacement + pathToCleanse.Replace("c:", "C$");
            }
            else if (pathToCleanse.StartsWith("d:"))
            {
                returnPath = pathReplacement + pathToCleanse.Replace("d:", "D$");
            }
            else if (pathToCleanse.StartsWith("e:"))
            {
                returnPath = pathReplacement + pathToCleanse.Replace("e:", "E$");
            }
            else if (pathToCleanse.StartsWith("f:"))
            {
                returnPath = pathReplacement + pathToCleanse.Replace("f:", "F$");
            }
            else if (pathToCleanse.StartsWith("g:"))
            {
                returnPath = pathReplacement + pathToCleanse.Replace("g:", "G$");
            }
            return returnPath;
        }
    }
}
