﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace MoveDatabases
{
    public class DatabaseService
    {
        private static SqlConnection CreateConnection(bool destinationDatabase)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["Source"].ConnectionString;
            if(destinationDatabase)
            {
                connectionString = ConfigurationManager.ConnectionStrings["Destination"].ConnectionString;
            }
            var db = new SqlConnection(connectionString);
            db.Open();
            return db;
        }

        private static SqlConnection CreateConnectionByXML()
        {
            XmlReaderSettings readerSettings = new XmlReaderSettings();
            readerSettings.IgnoreComments = true;
            var connString = string.Empty;
            using (XmlReader reader = XmlReader.Create(Assembly.GetExecutingAssembly().CodeBase + @"\..\..\Web.config", readerSettings))
            {
                reader.ReadToFollowing("connectionStrings");
                reader.ReadToFollowing("add");
                connString = reader.GetAttribute("connectionString");
            }
            var db = new SqlConnection(connString);
            db.Open();
            return db;
        }

        public static SqlCommand CreateCommand(bool destination = false)
        {
            var cmd = CreateConnection(destination).CreateCommand();

            cmd.Disposed += cmd_Disposed;
            cmd.CommandTimeout = ConfigurationManager.AppSettings.AllKeys.Contains("CommandTimeout") ? Convert.ToInt32(ConfigurationManager.AppSettings["CommandTimeout"]) : cmd.CommandTimeout;
            return cmd;
        }

        public static SqlCommand CreateCommandByXML()
        {
            var cmd = CreateConnectionByXML().CreateCommand();
            cmd.Disposed += cmd_Disposed;
            return cmd;
        }


       
        private static void cmd_Disposed(object sender, EventArgs e)
        {
            var cmd = (SqlCommand)sender;
            cmd.Connection.Close();
            cmd.Connection.Dispose();

            cmd.Disposed -= cmd_Disposed;
        }
       
    }
}
